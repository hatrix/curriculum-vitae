SHELL=bash

all: en fr cover

cover:
	xelatex coverletter.tex

en:
	xelatex -output-directory=english '\newif\ifen\newif\iffr\entrue\frfalse\input resume.tex' 
	xelatex -output-directory=english '\newif\ifen\newif\iffr\entrue\frfalse\input resume.tex' 

	#cp english/resume.pdf /var/www/html/legarrec.org/mael/cv_en.pdf

fr:
	xelatex -output-directory=french '\newif\ifen\newif\iffr\frtrue\enfalse\input resume.tex'
	xelatex -output-directory=french '\newif\ifen\newif\iffr\frtrue\enfalse\input resume.tex'

	#cp french/resume.pdf /var/www/html/legarrec.org/mael/cv_fr.pdf

clean:
	rm -f english/*.{aux,log,out,pdf}
	rm -f french/*.{aux,log,out,pdf}
